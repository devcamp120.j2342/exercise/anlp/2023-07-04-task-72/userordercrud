package com.devcamp.user_order_crud.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.user_order_crud.model.CUser;
import com.devcamp.user_order_crud.repository.UserRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CUserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/users")
    public List<CUser> getAllUser() {
        return userRepository.findAll();
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable(value = "id") long id) {
        try {
            Optional<CUser> optionalUser = userRepository.findById(id);
            if (optionalUser.isPresent()) {
                return new ResponseEntity<>(optionalUser.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/users")
    public ResponseEntity<Object> createdUser(@RequestBody CUser cUserCilent) {
        try {
            CUser user = new CUser();
            user.setAddress(cUserCilent.getAddress());
            user.setEmail(cUserCilent.getEmail());
            user.setFullname(cUserCilent.getFullname());
            user.setPhone(cUserCilent.getPhone());
            user.setUpdated(null);
            CUser saveUser = userRepository.save(user);
            return new ResponseEntity<>(saveUser, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable(value = "id") long id, @RequestBody CUser userUpdate) {
        try {
            Optional<CUser> userData = userRepository.findById(id);
            if (userData.isPresent()) {
                CUser newUser = userData.get();
                newUser.setFullname(userUpdate.getFullname());
                newUser.setAddress(userUpdate.getAddress());
                newUser.setEmail(userUpdate.getEmail());
                newUser.setPhone(userUpdate.getPhone());
                CUser saveUser = userRepository.save(newUser);
                return new ResponseEntity<>(saveUser, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("users/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable(value = "id") long id) {
        try {
            Optional<CUser> optional = userRepository.findById(id);
            if (optional.isPresent()) {
                userRepository.deleteById(id);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/count")
    public Long countUser() {
        return userRepository.count();
    }

    @GetMapping("/users/check/{id}")
    public Boolean checkUser(@PathVariable(value = "id") long id) {
        return userRepository.existsById(id);
    }

    @GetMapping("/users/page")
    public ResponseEntity<Object> getUserPage(
            @RequestParam(value = "page", defaultValue = "1") String page,
            @RequestParam(value = "size", defaultValue = "5") String size) {
        try {
            Pageable pageElement = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<CUser> list = new ArrayList<CUser>();
            userRepository.findAll(pageElement).forEach(list::add);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return null;
        }

    }
}
