package com.devcamp.user_order_crud.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.user_order_crud.repository.OrderRepository;
import com.devcamp.user_order_crud.model.COrder;

@CrossOrigin
@RestController
@RequestMapping("/")
public class COrderController {
    @Autowired
    private OrderRepository orderRepository;

    @GetMapping("/order/all")
    public List<COrder> getAllOrders() {
        return orderRepository.findAll();
    }

    @GetMapping("/order/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable(value = "id") long id) {
        try {
            Optional<COrder> order = orderRepository.findById(id);
            if (order.isPresent()) {
                return new ResponseEntity<>(order.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/{id}/orders")
    public List<COrder> getOrdersByUser(@PathVariable(value = "id") Long id) {
        return orderRepository.getOrderBycUserId(id);
    }

    @PostMapping("/order/create")
    public ResponseEntity<Object> createOrder(@RequestBody COrder orderCilent) {
        try {
            COrder order = new COrder();
            order.setOrderCode(orderCilent.getOrderCode());
            order.setPizzaSize(orderCilent.getPizzaSize());
            order.setPizzaType(orderCilent.getPizzaType());
            order.setPrice(orderCilent.getPrice());
            order.setCreated(LocalDateTime.now());
            order.setUpdated(null);
            orderRepository.save(order);
            return new ResponseEntity<>(order, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/order/update/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable(value = "id") long id, COrder orderUpdate) {
        try {
            Optional<COrder> orderData = orderRepository.findById(id);
            if (orderData.isPresent()) {
                COrder order = orderData.get();
                order.setOrderCode(orderUpdate.getOrderCode());
                order.setPizzaSize(orderUpdate.getPizzaSize());
                order.setPizzaType(orderUpdate.getPizzaType());
                order.setPrice(orderUpdate.getPrice());
                orderRepository.save(order);
                return new ResponseEntity<>(order, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/order/delete/{id}")
    public ResponseEntity<Object> deleteOrder(@PathVariable(value = "id") long id) {
        try {
            Optional<COrder> order = orderRepository.findById(id);
            if (order.isPresent()) {
                orderRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
