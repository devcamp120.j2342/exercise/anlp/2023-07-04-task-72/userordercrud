package com.devcamp.user_order_crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.user_order_crud.model.CUser;

@Repository
public interface UserRepository extends JpaRepository<CUser, Long>{
}
